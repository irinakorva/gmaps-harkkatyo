<?php
wp_enqueue_style('wpstyle');
?>

<div class="wrap">
	<?php
	global $wpdb;
	global $table_name;

	$description="";
	$latitude="";
	$longitude="";
	$table_name=$wpdb->prefix . "marker";
	if (isset($_POST["description"])){
	?>

	<div class='updated'><p>
	
	<?php
	$description=sanitize_text_field($_POST["description"]);
	$latitude=sanitize_text_field($_POST["latitude"]);
	$longitude=sanitize_text_field($_POST["longitude"]);

	$map = $wpdb->get_row("SELECT * FROM " . $table_name);

	if ($map==null) {
		$wpdb->insert (
			$table_name,
				array (
				'description' => $description,
				'latitude' => $latitude,
				'longitude' => $longitude
			)
		);
	}

	else {
		$id=$map->id;
		$wpdb->update(
			$table_name,
			array(
				'description' => $description,
				'latitude' => $latitude,
				'longitude' => $longitude
			),
		array ('id'=>$id)
		);
	}
	_e('Settings saved.',PLUGIN_NAME);
	?>
	</p>
</div>
<?php }

else {
	$map=$wpdb->get_row("SELECT * FROM " . $table_name);
	if ($map !=null) {
		$description=$marker->$description;
		$latitude=$marker->$latitude;
		$longitude=$marker->$longitude;
	}
}?>

	<h2><?php _e('Add location',PLUGIN_NAME);?></h2>
	
	<form method="post" action="">
		<table class="form-table">
			<tbody>
				<tr valign="top">
					<td scope="row">
						<label for="description"><?php _e('Description',PLUGIN_NAME);?>:</label>
					</td><br>
					<td>
						<input id="description" name='description' size='30' maxlength='50' value="<?php print($description);?>">
					</td>
				</tr>

				<tr valign="top">
					<td scope="row">
						<label for="latitude"><?php _e('Latitude',PLUGIN_NAME);?>:</label>
					</td><br>
					<td>
						<input id="latitude" name='latitude' size='12' maxlength='9' value="<?php print($latitude);?>">
					</td>
				</tr>

				<tr valign="top">
					<td scope="row">
						<label for="longitude"><?php _e('Longitude',PLUGIN_NAME);?>:</label>
					</td><br>
					<td>
						<input id="longitude" name='longitude' size='12' maxlength='9' value="<?php print($latitude);?>">
					</td>
				</tr>
			
			</tbody>
		</table>
		<input type='submit' class='button button-primary' value='<?php _e('Save',PLUGIN_NAME) ?>'>
	</form>
	
</div>