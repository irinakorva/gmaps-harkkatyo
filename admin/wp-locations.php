<?php
wp_enqueue_script('wpscript');
if (isset($_POST["id"])) {
	global $wpdb;
	$table_name=$wpdb->prefix . "marker";
	$idt=$_POST["id"];

	if (isset($_POST["action"])) {
		foreach ($idt as $id) {
			$id=intval($id);
			$temp=array("id" => $id);
			$wpdb->delete($table_name,$temp);
		}
		print "<div class='updated'><p>";
		_e('Selected locations deleted!',PLUGIN_NAME);
		print "</p></div>";
	}
}
?>
<div class="wrap">
	<h2><?php _e('Locations',PLUGIN_NAME);?></h2>

	<form method="post" action="">
	<div class="tablenav top">
		<div class="alignleft action">
			<select name="action">
				<option value="2"><?php _e('Delete',PLUGIN_NAME); ?></option>
			</select>
			<input type="submit" value="<?php _e('Accept',PLUGIN_NAME); ?>" class="button action">
		</div>
	</div>
	<table class="wp-list-table widefat">
		<thead>
			<tr>
				<th class="manage-column check-column"></th>
				<th class="column-title"><?php _e('Description',PLUGIN_NAME);?> </th>
				<th><?php _e('Latitude',PLUGIN_NAME); ?></th>
				<th><?php _e('Longitude',PLUGIN_NAME); ?></th>
			</tr>
		</thead>
		<tfoot>
			<tr class="">
				<th></th>
				<th><?php _e('Description',PLUGIN_NAME);?> </th>
				<th><?php _e('Latitude',PLUGIN_NAME); ?></th>
				<th><?php _e('Longitude',PLUGIN_NAME); ?></th>
			</tr>
		</tfoot>
		<tbody>
			<?php
			global $wpdb;
			$table_name=$wpdb->prefix . "marker";
			$sql="SELECT * FROM " . $table_name . " ORDER BY description";
			$locations=$wpdb->get_results($sql);

			if ($locations) {
				foreach ($locations as $marker) {
					print "<tr>";
                    print "<td><input type='checkbox' name='id[]' value='" . $marker->id . "'</td>";
                    print "<td>" . sanitize_text_field($marker->description);
                    print "<div class='row-actions'>";
                    print "<span class='view'><a href='#' id='$marker->id' class='submitdelete'>" . __('Delete',PLUGIN_NAME) . "</a></span>";
                    print "</div>";
                    print "</td>";
                    print "<td>" . sanitize_text_field($marker->description) . "</td>";
                    print "<td>" . sanitize_text_field($marker->latitude) . "</td>";
                    print "<td>" . sanitize_text_field($marker->longitude) . "</td>";

				}
			}
			?>
		</tbody>
	</table>
	<div class="tablenav bottom">
		<div class="alignleft action">
			<select name="action">
				<option value="2"><?php _e('Delete',PLUGIN_NAME); ?></option>
			</select>
			<input type="submit" value="<?php _e('Accept',PLUGIN_NAME); ?>" class="button action">	
		</div>
	</div>
	</form>
</div>