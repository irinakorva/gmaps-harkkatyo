<?php
class Wp_gmaps_tietokannan_luonti {
	public static function activate() {
		global $wpdb;
		$table_name1=$wpdb->prefix . "map";

		$sql = "CREATE TABLE IF NOT EXISTS $table_name1 (
				id int(11) PRIMARY KEY AUTO_INCREMENT,
				latitude varchar(9) NOT NULL,
				longitude varchar(9) NOT NULL,
				address varchar(50) NOT NULL,
				zip varchar(15) NOT NULL,
				city varchar(50) NOT NULL,
				active bool
		);";

		require_once(ABSPATH . "wp-admin/includes/upgrade.php");
		dbDelta($sql);

		$table_name2=$wpdb->prefix . "marker";
		$sql = "CREATE TABLE IF NOT EXISTS " . $table_name2 . "( ";
		$sql .= "id int(11) PRIMARY KEY AUTO_INCREMENT,";
		$sql .= "description varchar(50),";
		$sql .= "latitude varchar(9) NOT NULL,";
		$sql .= "longitude varchar(9) NOT NULL);";

		require_once(ABSPATH . "wp-admin/includes/upgrade.php");
		dbDelta($sql);
		
		add_option("wp-gmaps-db-version","0.1");
	}

	public static function uninstall() {
		global $wpdb;
		$table_name = $wpdb->prefix . "map";
		$sql = "DROP TABLE IF EXISTS $table_name;";
		$wpdb->query($sql);
		$table_name = $wpdb->prefix . "marker";
		$sql = "DROP TABLE IF EXISTS $table_name;";
		$wpdb->query($sql);
		delete_option("wp-gmaps-db-version");
	}
}
?>