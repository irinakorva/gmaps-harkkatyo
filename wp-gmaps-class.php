<?php
class Wp_gmaps_class {

	public function __construct() {
		load_plugin_textdomain(PLUGIN_NAME, false, basename( dirname(__FILE__)) . '/languages');
		
		add_action('init', array($this,'init'));
		add_action('widgets_init',array($this,'register_widget'));
		add_action('admin_menu',array($this,'setup_admin_menu'));
	}
	
	public function init() {
		wp_register_style('wpstyle', plugins_url('css/style.css',__FILE__));
	}
	
	public function setup_admin_menu() {
		add_object_page(PLUGIN_NAME,__('GMaps',PLUGIN_NAME),'manage_options',PLUGIN_NAME,array($this,'admin_page'));
		add_submenu_page(PLUGIN_NAME,__('Map',PLUGIN_NAME),__('Map',
		PLUGIN_NAME),'manage_options',PLUGIN_NAME,array($this,'admin_page'));
		add_submenu_page(PLUGIN_NAME,__('Locations',PLUGIN_NAME),__('Locations',
		PLUGIN_NAME),'manage_options','submenu',array($this,'admin_locations'));
		add_submenu_page(PLUGIN_NAME,__('Add location',PLUGIN_NAME),__('Add location',
		PLUGIN_NAME),'manage_options','submenu2',array($this,'admin_add'));
	}
	
	public function register_widget() {
		register_widget('Wp_gmaps_vimpain');
	}

	public function admin_page() {
		include_once(plugin_dir_path(__FILE__) . 'admin/wp-settings.php');
	}

	public function admin_locations() {
		include_once(plugin_dir_path(__FILE__) . 'admin/wp-locations.php');
	}

	public function admin_add() {
		include_once(plugin_dir_path(__FILE__) . 'admin/wp-addmarker.php');
	}
}

add_action('plugins_loaded','wp_gmaps_init');

function wp_gmaps_init() {
	$wp_plugin=new Wp_gmaps_class();
}

?>