<?php
/*
Plugin Name: Google Maps harkka
Plugin URI: http://www.foo.com
Description: Harjoitustyö Web-sovellusten arkkitehtuurit -opintojaksolle.
Version: 0.1
Author: Irina Korva
Author URI: http:/www.foo.com
License: GPL2
*/

define('PLUGIN_NAME','wp-plugari');

include_once('wp-gmaps-vimpain.php');
include_once('wp-gmaps-class.php');
include_once('wp-gmaps-tietokannan-luonti.php');

register_activation_hook(__FILE__,array('Wp_gmaps_tietokannan_luonti','activate'));
register_uninstall_hook(__FILE__,array('Wp_gmaps_tietokannan_luonti','uninstall'));
?>